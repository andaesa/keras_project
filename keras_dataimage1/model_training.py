from keras.models import Sequential
from keras.layers import Dense
from PIL import Image
import numpy as np

# fix random seed for reproductibility
seed = 7
np.random.seed(seed)

# Load data
# dataset = np.loadtxt("try8.csv", delimiter=",")
im = Image.open("8Greyscale.png")
#pixels = list(im.getdata())
pixels = np.array(im.getdata())

X = Y = pixels

model = Sequential()
model.add(Dense(12, input_dim=1, init='uniform', activation='relu'))
model.add(Dense(1, init='uniform', activation='sigmoid'))

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

model.fit(X, Y, nb_epoch=1000, batch_size=10)

scores = model.evaluate(X, Y)
print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

model_json = model.to_json()
with open("image1.json", "w") as json_file:
    json_file.write(model_json)

model.save_weights('image1.h5')