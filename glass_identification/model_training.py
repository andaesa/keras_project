"""from keras.models import Sequential
from keras.layers import Dense
import numpy

# fix random seed for reproducibility
seed = 7
numpy.random.seed(seed)

# load pima indians dataset
dataset = numpy.loadtxt("glass.csv", delimiter=",")
# split into input (X) and output (Y) variables
X = dataset[:,0:10].astype(float)
Y = dataset[:,10]

# create model
model = Sequential()
model.add(Dense(10, input_dim=10, init='uniform', activation='relu'))
model.add(Dense(10, init='uniform', activation='relu'))
model.add(Dense(1, init='uniform', activation='sigmoid'))

# Compile model
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# Fit the model
model.fit(X, Y, nb_epoch=10000, batch_size=10)

# evaluate the model
scores = model.evaluate(X, Y)
print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

model_json = model.to_json()
with open("glass.json", "w") as json_file:
    json_file.write(model_json)

model.save_weights('glass.h5')"""

import numpy
from pandas import read_csv
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

seed = 7
numpy.random.seed(seed)

dataframe = read_csv("glass.csv", header=None)
dataset = dataframe.values

X = dataset[:,0:10].astype(float)
Y = dataset[:,10]

encoder = LabelEncoder()
encoder.fit(Y)
encoded_Y = encoder.transform(Y)
#dummy_y = np_utils.to_categorical(encoded_Y)

def create_baseline():
	model = Sequential()
	model.add(Dense(10, input_dim=10, init='normal', activation='relu'))
	model.add(Dense(1, init='normal', activation='sigmoid'))

	model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

	model_json = model.to_json()
	with open("glass.json", "w") as json_file:
		json_file.write(model_json)

	model.save_weights('glass.h5')

	return model

estimator = KerasClassifier(build_fn=create_baseline, nb_epoch=1000, batch_size=10, verbose=0)
kfold = StratifiedKFold(n_splits=10, shuffle=True, random_state=seed)
results = cross_val_score(estimator, X, encoded_Y, cv=kfold)
print("Baseline: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))


