# Create first network with Keras
from keras.models import Sequential, load_model, model_from_json
from keras.layers import Dense
import numpy

# fix random seed for reproducibility
seed = 9
numpy.random.seed(seed)

# load pima indians dataset
dataset = numpy.loadtxt("glass.csv", delimiter=",")

# split into input (X) and output (Y) variables
X = dataset[:,0:10].astype(float)
# Y = dataset[:,8]

json_file = open('glass.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("glass.h5")
print("Loaded model from disk")

# calculate predictions
predictions = loaded_model.predict(X)

# round predictions
rounded = [round(x) for x in predictions]
print(rounded)
