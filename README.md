This repository contains the testing of training neural network model. It use dataset from this website : https://archive.ics.uci.edu/ml/index.html

**1.Iris Dataset**

The original dataset consists of 5 column. The 5th column is the output consists of 3 classes.
The edited dataset consists of 6 column. The new added column is the data for the height of the plant. (Reminder: This new added column is not real data)
Also, the output has been changed to 4 classes by adding 'iris-West'
Train this model by running the model_training.py script.

**2.Pima-Indians-diabetes Dataset**

This is the basic tutorial to train neural network model. The original dataset has integer output of 1 and 0.
The edited dataset is to be use when predicting the model.

**3.Sonar Object Dataset**

This is a dataset that describes sonar chirp returns bouncing of different surfaces (R = Rock and M = Mine) 

**4.Glass Identification Dataset**

This dataset is to identify what type of glass.
The original dataset was trained by using example training script from Pima-Indians-diabetes. The accuracy result is 32.71%.
Changes has been made to the output of the dataset from integer of 1-7 to alphabet a-f. Then I train the model based on the example script from
Iris Dataset and Sonar Dataset because of the same form of outut which is in string. The result is as follows:

The estimated accuracy of the model in the form of mean = 68.42% and Standard Deviation = 3.03%

5.**convert_project**

This project is to convert image and audio into the form of array of float. The next step is to determined how to use this array as input to train neural network model.

##################################################################################
**[Issue]**

Based on the result obtained from glass identification dataset. I am still not unsure on how to determine what is the caused of the different result just by changing the output parameters
from integer to string. I am also not very clear regarding how many layers and how many neurons one should use in his/her model to train specific dataset.
This probably has something to do with what backend that one use to train (Tensorflow or Theano) but there is no clear evident of that.

I will do some other testing regarding this and will update the result.


Other issue is, all above test has only got same kind of output. Further testing/experiment should be done with dataset that has other form of output.
Meaning that, for example output that are in float parameters, output that have range and others.


**[Update 14 January 2017]**

Added 2 new directory.

**1.keras_dataimage1**

In this directory is my attempt to put an array of image pixel as input to train a model. I tried to convert an image using the script from directory "convert_project" and saved it
as .csv file. My first attempt is to use example script from "pima-indians-diabetes" to train this model. However, when I checked the csv file, there is only one column and one row where
inside it is the data for the converted image. (See "try8.csv")
Thus it made me hard to determined the input and output of the model.

To solve this I wrote the script for the model just like in "model_training.py". I load the image, get the pixel's data and set it to be X and Y variable.
However, when I train this model, the result is not what I expected. The accuracy is 0%.

Reason for this to happen might have to do with the dataset. I only input one image that has been converted to array. I conclude that I must convert a lot more image in the form of array and save it in csv file to be use with the model_training script.  

**2.keras_dataimage2**

From directory "keras_dataimage1", I conclude that it must have something to do with how many image should be use to train the model. I searched through the internet
and found this link : https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html

Using example from above link and some sample image that you can found at subdirectory "data", I use 20 image as validation for each subdirectory 'amad' and 'mabo' and the rest is for train for each subdirectory 'amad' and 'mabo'.I am able to train the model and get around 90% to 100% accuracy.
At this moment I am searching for a way to load the weight generated from the training and use it on an image to check if the computer is really understand the image or not.